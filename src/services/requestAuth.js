import axios from "axios"
import nookies from "nookies"

const { token } = nookies.get("token")
export const requestAuth = axios.create({
  baseURL: process.env.NEXT_PUBLIC_AUTH_BASE_URL,
  headers: {
    Authorization: token ? `Bearer ${token}` : "API-KEY",
    ["x-api-key"]: "P-ay62PT3mB9Li1JflbRvBAFCaCP3fdolE",
  },
})
