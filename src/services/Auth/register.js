import { requestAuth } from "../requestAuth"
import { useMutation, useQuery } from "react-query"

const create = (data) => requestAuth.post("/register-email-otp/user", data)

export const useRegister = () => {
  const createRegister = useMutation(create)

  return {
    createRegister,
  }
}
