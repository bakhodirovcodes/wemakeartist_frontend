import { useMutation } from "react-query"
import { request } from "./http-client"

const comment = {
  create: (data) => request.post(`/v1/object/comment`, data),
}

export const useCommentMutation = (mutationSettings) => {
  return useMutation((data) => comment.create(data), mutationSettings)
}
