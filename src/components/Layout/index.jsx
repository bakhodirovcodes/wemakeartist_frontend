import nookies from "nookies"

import Footer from "../UI/Footer/Footer"
import Header from "@/components/UI/Header/Header"
import CustomSections from "../UI/CustomSections/CustomSection"
import Password from "../UI/Password/Password"

export default function Layout({ children }) {
  const { password } = nookies.get("password")
  return (
    <>
      {password === "Udevs123!" ? (
        <>
          <Header />

          {children}
          <Footer />
        </>
      ) : (
        <CustomSections>
          <Password />
        </CustomSections>
      )}
    </>
  )
}
