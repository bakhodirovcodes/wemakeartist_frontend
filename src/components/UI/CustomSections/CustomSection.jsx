import { Container } from "@mui/material"
import styles from "./styles.module.scss"

const ColorValue = {
  1: styles.section,
}

const CustomSections = (props) => {
  const { children, changeColor } = props
  return (
    <section className={ColorValue[changeColor]}>
      <Container>{children}</Container>
    </section>
  )
}

export default CustomSections
