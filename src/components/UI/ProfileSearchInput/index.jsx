import { styled } from "@mui/material/styles"
import MuiTextField from "@mui/material/TextField"
import InputAdornment from "@mui/material/InputAdornment"

import { HeaderSearchIcon } from "/public/icons/icons"

const TextField = styled((props) => (
  <MuiTextField
    InputProps={{
      startAdornment: (
        <InputAdornment position="start">
          <HeaderSearchIcon />
        </InputAdornment>
      ),
    }}
    {...props}
  />
))({
  width: "100%",
  "& .MuiOutlinedInput-root": {
    paddingLeft: "16px",
    borderRadius: "0",
    color: "white",
    backgroundColor: "rgba(255, 255, 255, 0.15);",
    "&.Mui-focused": {
      backgroundColor: "transparent",
    },
    "& input": {
      padding: "10px 64px 10px 8px",
      fontSize: "15px",
      lineHeight: "18px",
    },
    "& fieldset": {
      borderColor: "transparent",
    },
    "&:hover fieldset": {
      borderColor: "transparent",
    },
    "&.Mui-focused fieldset": {
      borderWidth: "1px",
      borderColor: "white",
      backgroundColor: "rgba(255, 255, 255, 0.15);",
    },
  },
})

function ProfileSearchInput() {
  return <TextField placeholder="Search for courses and authors" />
}

export default ProfileSearchInput
