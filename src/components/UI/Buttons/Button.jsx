import useTranslation from "next-translate/useTranslation"
import styles from "./styles.module.scss"

const Button = (props) => {
  const {
    type = "button",
    title,
    className,
    textClassName,
    onClick = () => {},
    changeBorder = false,
  } = props
  const { t } = useTranslation()

  return (
    <button
      type={type}
      className={className || !changeBorder ? styles.button : styles.btn}
      onClick={onClick}
    >
      {title && (
        <span className={textClassName || styles.text}>{t(title)}</span>
      )}
    </button>
  )
}

export default Button
