export const treningItems = [
  {
    id: "1",
    img: "/images/treaning/treaningImg.png",
    title: "khabib nurmagomedov",
    link: "/trainers",
  },
  {
    id: "2",
    img: "/images/treaning/treaningImg2.png",
    title: "DAVID BENAVIDEZ",
    link: "/trainers",
  },
  {
    id: "3",
    img: "/images/treaning/treaningImg3.png",
    title: "ALEXANDER USKY",
    link: "/trainers",
  },
]

export const treningItemsWithCategory = [
  {
    id: "1",
    img: "/images/treaning/treaningImg.png",
    title: "khabib nurmagomedov",
    link: "/trainers",
  },
  {
    id: "2",
    img: "/images/treaning/treaningImg2.png",
    title: "DAVID BENAVIDEZ",
    link: "/trainers",
  },
  {
    id: "3",
    img: "/images/treaning/treaningImg3.png",
    title: "alexander usky",
    link: "/trainers",
  },
  {
    id: "4",
    img: "/images/treaning/treaningImg4.png",
    title: "Buakaw Banchamek",
    link: "/trainers",
  },
  {
    id: "5",
    img: "/images/treaning/treaningImg5.png",
    title: "kamaru usman",
    link: "/trainers",
  },
  {
    id: "6",
    img: "/images/treaning/treaningImg6.png",
    title: "ISRAEL ADESANYA",
    link: "/trainers",
  },
]
