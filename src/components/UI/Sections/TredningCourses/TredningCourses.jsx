import { useRouter } from "next/router"

import CoursesCard from "@/components/CoursesCard/CoursesCard"
import { treningItems, treningItemsWithCategory } from "./treningItems"
import styles from "./styles.module.scss"

const TredningCourses = () => {
  const router = useRouter()

  return (
    <div className={styles.tredningBox}>
      <div className={styles.header}>
        <h2 className={styles.header__title}>Tredning courses</h2>
      </div>
      <div className={styles.boxes}>
        {router.pathname === "/category" ? (
          <>
            {treningItemsWithCategory.map((el) => (
              <CoursesCard
                key={el.id}
                img={el.img}
                title={el.title}
                link={el.link}
              />
            ))}
          </>
        ) : (
          <>
            {treningItems.map((items) => (
              <CoursesCard
                key={items.id}
                img={items.img}
                title={items.title}
                link={items.link}
              />
            ))}
          </>
        )}
      </div>
    </div>
  )
}

export default TredningCourses
