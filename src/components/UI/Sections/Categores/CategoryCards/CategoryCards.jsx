import styles from "./styles.module.scss"

const CategoryCards = ({ text }) => {
  return (
    <div className={styles.card}>
      <p className={styles.card__text}>{text}</p>
    </div>
  )
}

export default CategoryCards
