export const categoryItems = [
  {
    id: "1",
    text: "Karate",
  },
  {
    id: "2",
    text: "Taekwondo",
  },
  {
    id: "3",
    text: "Judo",
  },
  {
    id: "4",
    text: "Brazilian Jiu-Jitsu",
  },
  {
    id: "5",
    text: "Muay Thai",
  },
  {
    id: "6",
    text: "Boxing",
  },
  {
    id: "7",
    text: "Kickboxing",
  },
  {
    id: "8",
    text: "MMA",
  },
]
