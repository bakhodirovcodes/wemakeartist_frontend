import CategoryCards from "./CategoryCards/CategoryCards"
import { categoryItems } from "./categoryItems"
import styles from "./styles.module.scss"

const Categores = () => {
  return (
    <div className={styles.categores}>
      <div className={styles.header}>
        <h2 className={styles.header__title}>
          Choose a category to watch a class highlight
        </h2>
        <p className={styles.header__text}>
          UNLIMITED ACCSES TO ALL WORKSHOPS AND FIGHTER
        </p>
      </div>
      <div className={styles.categoryBoxes}>
        {categoryItems.map((items) => (
          <CategoryCards key={items.id} text={items.text} />
        ))}
      </div>
    </div>
  )
}

export default Categores
