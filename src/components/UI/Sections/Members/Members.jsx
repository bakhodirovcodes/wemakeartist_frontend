import CoursesCard from "@/components/CoursesCard/CoursesCard"
import { membersItems } from "./membersItems"
import styles from "./styles.module.scss"
const Members = () => {
  return (
    <div className={styles.members}>
      <div className={styles.header}>
        <h2 className={styles.header__title}>
          members who liked this class also liked
        </h2>
      </div>
      <div className={styles.cards}>
        {membersItems.map((item) => (
          <CoursesCard
            key={item.id}
            img={item.img}
            title={item.title}
            link={item.link}
          />
        ))}
      </div>
    </div>
  )
}

export default Members
