import styles from "./styles.module.scss"
const LeftContent = () => {
  return (
    <div className={styles.content}>
      <div className={styles.video}>
        <iframe
          src="https://www.youtube.com/embed/_Q5e4eEYHWM"
          title="YouTube video player"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
        />
      </div>
      <p className={styles.text}>
        Khabib Nurmagomedov, the undisputed champion, stands as a beacon of
        excellence in the fighting world. Now, aspiring fighters have the unique
        opportunity to learn from the best. Through his online workshops, Khabib
        shares his winning techniques, unrivaled skills, and the mindset of a
        true champion. Elevate your fighting game and unlock your potential
        under his guidance.
      </p>
      <div className={styles.instruction}>
        <ul>
          <li>Instructor(s) :</li>
          <li>Class lenght :</li>
          <li>Category :</li>
        </ul>
        <ul>
          <li>Khabib Nurmagamedov</li>
          <li>12 video lessons ( 3 hours 33 minutes)</li>
          <li>MMA</li>
        </ul>
      </div>
    </div>
  )
}

export default LeftContent
