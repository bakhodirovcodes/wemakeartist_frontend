export const rightContentItems = [
  {
    id: 1,
    text: "1. Meet your insturctor",
  },
  {
    id: 2,
    text: "2. Mastering the Art of Wrestling",
  },
  {
    id: 3,
    text: "3. Dominating Ground Control",
  },
  {
    id: 4,
    text: "4. Developing Superior Takedowns",
  },
  {
    id: 5,
    text: "5. Implementing Effective Ground and Pound",
  },
  {
    id: 6,
    text: "6. Applying Smothering Pressure",
  },
  {
    id: 7,
    text: "7. Defending Against Strikers",
  },
  {
    id: 8,
    text: "8. Utilizing Effective Cage Control",
  },
  {
    id: 9,
    text: "9.  Mental Strength and Focus",
  },
  {
    id: 10,
    text: "10. Tactical Fight Planning",
  },
  {
    id: 11,
    text: "11. Championship Mindset",
  },
]
