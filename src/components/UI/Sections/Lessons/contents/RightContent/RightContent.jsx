import { PlayerIcon, ArrowDwIcon } from "/public/icons/icons"
import { rightContentItems } from "./rightContentItems"
import styles from "./styles.module.scss"

const RightContent = () => {
  return (
    <div className={styles.content}>
      <ul className={styles.classes}>
        <li className={styles.list}>
          <PlayerIcon /> <span>Class trailer</span>
        </li>
        <li className={styles.list}>
          <PlayerIcon /> <span>Class Sample</span>
        </li>
      </ul>

      <p className={styles.lessons}>Lesson plan</p>
      {rightContentItems.map((items) => (
        <div key={items.id} className={styles.plans}>
          <span>{items.text}</span>
          <span>
            <ArrowDwIcon />
          </span>
        </div>
      ))}
      <p className={styles.resText}>
        Khabib Nurmagomedov, the undisputed champion, stands as a beacon of
        excellence in the fighting world. Now, aspiring fighters have the unique
        opportunity to learn from the best. Through his online workshops, Khabib
        shares his winning techniques, unrivaled skills, and the mindset of a
        true champion. Elevate your fighting game and unlock your potential
        under his guidance.
      </p>
    </div>
  )
}

export default RightContent
