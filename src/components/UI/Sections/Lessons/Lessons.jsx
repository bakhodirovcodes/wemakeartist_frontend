import LeftContent from "./contents/LeftContent/LeftContent"
import RightContent from "./contents/RightContent/RightContent"
import styles from "./styles.module.scss"

const Lessons = () => {
  return (
    <div className={styles.container}>
      <div className={styles.leftContent}>
        <LeftContent />
      </div>
      <div className={styles.rightContent}>
        <RightContent />
      </div>
    </div>
  )
}

export default Lessons
