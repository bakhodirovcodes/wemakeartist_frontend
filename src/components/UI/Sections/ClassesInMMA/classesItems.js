export const classesItems = [
  {
    id: 1,
    img: "/images/classes/classesImg.png",
    text: "Karate",
  },
  {
    id: 2,
    img: "/images/classes/classesImg1.png",
    text: "Taekwondo",
  },
  {
    id: 3,
    img: "/images/classes/classesImg2.png",
    text: "Judo",
  },
]
