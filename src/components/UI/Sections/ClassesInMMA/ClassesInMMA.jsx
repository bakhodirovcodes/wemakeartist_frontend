import ClassesCard from "./ClassesCard/ClassesCard"
import { classesItems } from "./classesItems"
import styles from "./styles.module.scss"

const ClassesInMMA = () => {
  return (
    <div className={styles.mmaClasses}>
      <div className={styles.header}>
        <h2 className={styles.header__title}>Other Classes in MMA</h2>
      </div>
      <div className={styles.cards}>
        {classesItems.map((el) => (
          <ClassesCard key={el.id} img={el.img} text={el.text} />
        ))}
      </div>
    </div>
  )
}

export default ClassesInMMA
