import styles from "./styles.module.scss"

const ClassesCard = ({ img, text }) => {
  return (
    <div className={styles.card}>
      <img src={img} alt="cardImg" />
      <p className={styles.text}>{text}</p>
    </div>
  )
}

export default ClassesCard
