import OutlinedButton from "@/components/UI/Buttons/OutlinedBtn"
import styles from "./styles.module.scss"

const Box = () => {
  return (
    <div className={styles.box}>
      <p className={styles.box__title}>
        UNLIMITED ACCSES TO ALL WORKSHOPS AND FIGHTER
      </p>
      <ul className={styles.box_items}>
        <li>12 months access to all fighter and courses</li>
        <li>Theroy & practice combined</li>
      </ul>
      <OutlinedButton
        type="submit"
        className={styles.btn}
        textCLassName={styles.text}
        title="39,99€/month"
      />
    </div>
  )
}

export default Box
