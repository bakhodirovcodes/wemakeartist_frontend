import Box from "./Box/Box"
import styles from "./styles.module.scss"

const BuyAccess = () => {
  return (
    <div className={styles.box}>
      <img src="/images/buyaccess/buyaccess.png" alt="img" />
      <img src="/images/buyaccess/accessImg.jpg" alt="boxImg" />
      <div className={styles.box__info}>
        <Box />
      </div>
    </div>
  )
}

export default BuyAccess
