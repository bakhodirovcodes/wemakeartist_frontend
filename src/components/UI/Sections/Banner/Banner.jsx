import OutlinedButton from "../../Buttons/OutlinedBtn"
import styles from "./styles.module.scss"

const Banner = () => {
  return (
    <div className={styles.banner}>
      <div className={styles.banner__responseBannerInfo}>
        <h1>THE WORLD´S STRONGEST FIGHTING MENTROING</h1>
        <p>Access Exclusive Online Workshops by the world’s best Fighters</p>
        <OutlinedButton
          type="submit"
          className={styles.btn}
          textCLassName={styles.text}
          title="SECURE ACCESS NOW!"
        />
      </div>
      <img className={styles.resImg} src="/images/resBanner2.png" alt="img" />
      <img className={styles.img} src="/images/banner.png" alt="banner" />
      <div className={styles.banner__bannerInfo}>
        <h1>THE WORLD´S STRONGEST FIGHTING MENTROING</h1>
        <p>Access Exclusive Online Workshops by the world’s best Fighters</p>
        <OutlinedButton
          type="submit"
          className={styles.btn}
          textCLassName={styles.text}
          title="SECURE ACCESS NOW!"
        />
      </div>
    </div>
  )
}

export default Banner
