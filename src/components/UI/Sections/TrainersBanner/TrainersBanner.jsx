import styles from "./styles.module.scss"
const TrainersBanner = () => {
  return (
    <div className={styles.root}>
      <img src="/images/trannersResImg.png" alt="resImg" />
      <img src="/images/TranersBanner.png" alt="traniersImg" />

      <div className={styles.bannerInfo}>
        <h1 className={styles.bannerInfo__title}>khabib nurmagomedov</h1>
        <p className={styles.bannerInfo__text}>
          Master the Art of Dominance: Join the Exclusive Workshop with Khabib
          Nurmagomedov
        </p>
      </div>
    </div>
  )
}

export default TrainersBanner
