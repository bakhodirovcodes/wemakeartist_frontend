import OutlinedButton from "../../Buttons/OutlinedBtn"
import styles from "./styles.module.scss"

const GetUnlimitedAccess = () => {
  return (
    <div className={styles.access}>
      <div className={styles.header}>
        <h2 className={styles.header__title}>
          GET UNLIMITED ACCESS TO ALL WORKSHOPS
        </h2>
        <p className={styles.header__text}>Improve your skills with the best</p>
      </div>
      <div className={styles.accessInfo}>
        <img src="/images/access/accessImg.png" alt="infoImg" />
        <div className={styles.items}>
          <h1 className={styles.items__title}>
            get better and train like the champions
          </h1>
          <p className={styles.items__text}>
            Are you looking for a way to get in shape, learn self-defense, and
            improve your overall fitness? If so, then an MMA training course may
            be the perfect option for you.
          </p>
          <OutlinedButton
            type="submit"
            className={styles.btn}
            textCLassName={styles.text}
            title="Buy now"
          />
        </div>
      </div>
    </div>
  )
}

export default GetUnlimitedAccess
