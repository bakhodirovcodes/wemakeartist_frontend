import Link from "next/link"

import styles from "./styles.module.scss"

const HoverMenu = ({ menu }) => {
  return (
    <div className={styles.root}>
      <ul className={styles.subMenu}>
        {Array.isArray(menu) &&
          menu.map((item) => (
            <li className={styles.subItems} key={item.id}>
              <Link href={item.link} key={item.id}>
                <a className={styles.text}>{item.text}</a>
              </Link>
            </li>
          ))}
      </ul>
    </div>
  )
}

export default HoverMenu
