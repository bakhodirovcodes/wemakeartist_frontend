export const headerItems = [
  {
    id: 1,
    text: "All categories",
    link: "/",
    isMenu: true,
    subMenu: [
      {
        id: "sub1",
        text: "Karate",
        link: "/category",
      },
      {
        id: "sub2",
        text: "MMA",
        link: "/category",
      },
      {
        id: "sub2",
        text: "Judo",
        link: "/category",
      },
      {
        id: "sub2",
        text: "Brazilian Jiu-Jitsu",
        link: "/category",
      },
      {
        id: "sub2",
        text: "Muay Thai",
        link: "/category",
      },
      {
        id: "sub2",
        text: "Boxing",
        link: "/category",
      },
      {
        id: "sub2",
        text: "Kickboxing",
        link: "/category",
      },
      {
        id: "sub2",
        text: "Taekwondo",
        link: "/category",
      },
    ],
  },
  {
    id: 2,
    text: "Trainers",
    link: "/",
    isMenu: false,
    //  subMenu: [
    //    {
    //      text: "Видео",
    //      link: "/video",
    //    },
    //    {
    //      text: "фото",
    //      link: "/photo",
    //    },
    //  ],
  },
]
