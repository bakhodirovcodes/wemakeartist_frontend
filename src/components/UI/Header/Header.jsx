import { useState } from "react"
import { Container } from "@mui/material"
import useTranslation from "next-translate/useTranslation"
import Headroom from "react-headroom"
import Link from "next/link"

import DrawerItem from "./Drawer/DrawerItem"
import { MenuIcon, ResSeachIcon } from "/public/icons/icons"
import { headerItems } from "./headerItems"
import HoverMenu from "./HoverMenu/HoverMenu"
import OutlinedButton from "../Buttons/OutlinedBtn"
import ProfileSearchInput from "../ProfileSearchInput"
import styles from "./style.module.scss"

const Header = () => {
  const { t } = useTranslation()

  const [openDrawer, setOpenDrawer] = useState(false)

  const handleOpenDrawer = () => setOpenDrawer(true)
  const handleCloseDrawer = () => setOpenDrawer(false)

  return (
    <Headroom>
      <header className={styles.header}>
        <Container>
          <div className={styles.box}>
            <div className={styles.links}>
              <Link href="/">
                <a className={styles.logo}>
                  <img src="/images/logoImg.png" alt="logo" />
                </a>
              </Link>
              <nav>
                <ul className={styles.menu}>
                  {headerItems.map((items) => (
                    <li className={styles.menuItems} key={items.id}>
                      <Link href={items.link}>
                        <a className={styles.text}>{items.text}</a>
                      </Link>
                      {items.isMenu && (
                        <div className={styles.subMenu}>
                          <div>{<HoverMenu menu={items.subMenu} />}</div>
                        </div>
                      )}
                    </li>
                  ))}
                </ul>
              </nav>
              <div className={styles.searchInput}>
                <ProfileSearchInput />
              </div>
            </div>
            <div className={styles.buttons}>
              <OutlinedButton
                className={styles.outlined}
                textCLassName={styles.text}
                title={t("Log in")}
              />
              <OutlinedButton
                type="submit"
                className={styles.btn}
                textCLassName={styles.text}
                title={t("Book a demo")}
              />
            </div>
            <div className={styles.responeHeaderButtons}>
              <ResSeachIcon />
              <span onClick={() => handleOpenDrawer()}>
                <MenuIcon />
              </span>
            </div>
          </div>
        </Container>
        <DrawerItem open={openDrawer} onClose={handleCloseDrawer} />
      </header>
    </Headroom>
  )
}

export default Header
