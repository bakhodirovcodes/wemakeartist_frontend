import { Drawer } from "@mui/material"
import Link from "next/link"
import { useRouter } from "next/router"

import { headerItems } from "../headerItems"
import { CloseIcon, ArrowDwIcon } from "/public/icons/icons"
import {
  useStyles,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  customStyles,
} from "./customDrawer"
import styles from "./styles.module.scss"

const DrawerItem = ({ open, onClose }) => {
  const classes = useStyles()
  const router = useRouter()

  return (
    <Drawer
      open={open}
      onClose={onClose}
      anchor="right"
      className={classes.root}
    >
      <div className={styles.box}>
        <div className={styles.header}>
          <Link href="/">
            <a>
              <img src="/images/logoImg.png" alt="logo" />
            </a>
          </Link>
          <div onClick={onClose}>
            <CloseIcon />
          </div>
        </div>
        <div className={styles.boxBody}>
          <nav>
            <ul className={styles.menu}>
              {headerItems.map((items) => (
                <li className={styles.menuItems} key={items.id}>
                  {items.subMenu ? (
                    <Accordion sx={customStyles}>
                      <AccordionSummary expandIcon={<ArrowDwIcon />}>
                        {items.text}
                      </AccordionSummary>

                      {items.subMenu.map((el) => (
                        <AccordionDetails key={el.id}>
                          <h2
                            key={el.id}
                            className={styles.menuTexts}
                            onClick={() => {
                              router.push(el.link), onClose()
                            }}
                          >
                            {el.text}
                          </h2>
                        </AccordionDetails>
                      ))}
                    </Accordion>
                  ) : (
                    <Link href={items.link} key={items.id}>
                      <a className={styles.text}>{items.text}</a>
                    </Link>
                  )}
                </li>
              ))}
            </ul>
          </nav>
        </div>
      </div>
    </Drawer>
  )
}

export default DrawerItem
