import { styled } from "@mui/material/styles"
import MuiAccordion from "@mui/material/Accordion"
import MuiAccordionSummary from "@mui/material/AccordionSummary"
import MuiAccordionDetails from "@mui/material/AccordionDetails"
import { makeStyles } from "@mui/styles"

export const Accordion = styled((props) => (
  <MuiAccordion disableGutters elevation={0} square {...props} />
))({
  "&:not(:last-child)": {
    borderBottom: 0,
  },
  "&:before": {
    display: "none",
  },
})

export const AccordionSummary = styled((props) => {
  return <MuiAccordionSummary {...props} />
})({
  "& .MuiAccordionSummary-expandIconWrapper.Mui-expanded": {
    transform: "rotate(180deg)",
  },
})

export const AccordionDetails = styled(MuiAccordionDetails)({
  marginTop: "10px",
  padding: "10px",
})

export const useStyles = makeStyles({
  root: {
    background: "rgba(0, 0, 0, 0.1)",

    "& .MuiPaper-root": {
      width: "50%",
      background: "#000",

      ["@media (max-width: 768px)"]: {
        width: "100%",
      },
    },
    "& .MuiOutlinedInput-notchedOutline": {
      border: "1px solid rgba(0, 0, 0, 0.12) !important",
    },
  },
})

export const customStyles = {
  ".MuiAccordionSummary-root": {
    padding: 0,
    minHeight: 0,
    display: "flex",
  },
  ".MuiAccordionSummary-content": {
    margin: 0,
    fontWeight: 500,
    fontSize: "15px",
    lineHeight: "18px",
    color: "white",
  },
}
