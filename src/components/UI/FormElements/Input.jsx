import { Controller } from "react-hook-form"

import styles from "./styles.module.scss"

const Input = ({
  type = "text",
  disabled,
  name = "",
  defaultValue,
  control,
  icon,
  endIcon,
  errors,
  validation = false,
  validationSingle,
  autoComplete = "off",
  required = false,
  placeholder,
  inputDateProps = {},
  buyForMeBg,
  endAdornment = "",
  ...restProps
}) => {
  return (
    <div>
      <Controller
        control={control}
        rules={
          validation
            ? {
                required: {
                  value: validation,
                  message: "Required",
                },
              }
            : validationSingle
        }
        defaultValue={defaultValue}
        name={name}
        render={({ field: { value, onChange, name } }) => {
          return (
            <div
              style={
                errors?.[`${name}`]?.message ? { borderColor: "#F76659" } : {}
              }
              className={`${styles.inputWrapper} ${
                disabled && styles.disabled
              }`}
            >
              {icon && icon}
              <input
                className={styles[buyForMeBg]}
                placeholder={placeholder}
                type={type}
                disabled={disabled}
                value={value}
                autoComplete={autoComplete}
                onChange={onChange}
                required={required}
                {...restProps}
              />
              {endIcon && endIcon}
            </div>
          )
        }}
      />
      {errors?.[`${name}`]?.message && (
        <span className={styles.errMsg}>{errors?.[`${name}`]?.message}</span>
      )}
    </div>
  )
}

export default Input
