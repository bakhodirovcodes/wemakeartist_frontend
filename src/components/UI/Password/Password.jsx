import { useForm } from "react-hook-form"
import { setCookie } from "nookies"
import nookies from "nookies"

import OutlinedButton from "../Buttons/OutlinedBtn"
import Frow from "../FormElements/FRow"
import Input from "../FormElements/Input"
import styles from "./styles.module.scss"
import { useEffect } from "react"

const Password = () => {
  const { password } = nookies.get("password")
  const {
    control,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    defaultValues: {
      password: "",
    },
  })

  const onSubmit = (data) => {
    if (data.password == "Udevs123!") {
      window.location.replace("/")
    }
  }

  useEffect(() => {
    if (!password)
      setCookie(null, "password", "Udevs123!", {
        path: "/",
      })
  }, [password])

  return (
    <div className={styles.box}>
      <form onSubmit={handleSubmit(onSubmit)} className={styles.form}>
        <Frow label="Password">
          <Input
            name="password"
            type="password"
            control={control}
            placeholder="Please enter password"
            validation={true}
            errors={errors}
          />
        </Frow>
        <OutlinedButton
          type="submit"
          className={styles.btn}
          textCLassName={styles.text}
          title="Log in"
        />
      </form>
    </div>
  )
}

export default Password
