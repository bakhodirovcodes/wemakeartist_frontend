import { Container } from "@mui/material"

import {
  AppstorIcon,
  GooglePlayIcon,
  AmazonAppStoreIcon,
} from "/public/icons/icons"
import styles from "./style.module.scss"

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <Container>
        <div className={styles.box}>
          <div className={styles.items}>
            <ul>
              <li>Explore</li>
              <li>Articles</li>
              <li>Sitemap</li>
              <li>Gifts</li>
            </ul>
          </div>
          <div className={styles.items}>
            <ul>
              <li>About</li>
              <li>Diversity, Equity, and Inclusion</li>
              <li>Security</li>
              <li>Privacy</li>
              <li>Terms of service </li>
              <li>For business</li>
              <li>For business</li>
            </ul>
          </div>
          <div className={styles.items}>
            <ul>
              <li>Social</li>
              <li>Twitter</li>
              <li>Instagram</li>
              <li>Facebook</li>
              <li>LinkedIn</li>
            </ul>
          </div>
          <div className={styles.items}>
            <ul>
              <li>Download</li>
              <li>
                <AppstorIcon />
              </li>
              <li>
                <GooglePlayIcon />
              </li>
              <li>
                <AmazonAppStoreIcon />
              </li>
            </ul>
          </div>
        </div>
      </Container>
    </footer>
  )
}

export default Footer
