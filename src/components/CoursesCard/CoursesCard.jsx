import { useRouter } from "next/router"

import styles from "./styles.module.scss"

const CoursesCard = ({ img, title, link }) => {
  const router = useRouter()
  return (
    <div className={styles.box} onClick={() => router.push(link)}>
      <img src={img} alt="img" />
      <p className={styles.text}>{title}</p>
    </div>
  )
}

export default CoursesCard
