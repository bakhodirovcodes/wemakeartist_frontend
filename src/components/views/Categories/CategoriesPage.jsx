import CustomSections from "@/components/UI/CustomSections/CustomSection"
import BuyAccess from "@/components/UI/Sections/BuyAccess/BuyAccess"
import TredningCourses from "@/components/UI/Sections/TredningCourses/TredningCourses"

const CategoriesPage = () => {
  return (
    <div>
      <CustomSections changeColor={1}>
        <TredningCourses />
      </CustomSections>
      <CustomSections changeColor={1}>
        <BuyAccess />
      </CustomSections>
    </div>
  )
}

export default CategoriesPage
