import CustomSections from "@/components/UI/CustomSections/CustomSection"
import Banner from "@/components/UI/Sections/Banner/Banner"
import GetUnlimitedAccess from "@/components/UI/Sections/GetUnlimitedAccess/GetUnlimitedAccess"
import TredningCourses from "@/components/UI/Sections/TredningCourses/TredningCourses"
import BuyAccess from "@/components/UI/Sections/BuyAccess/BuyAccess"
import Categores from "@/components/UI/Sections/Categores/Categores"

export function Main() {
  return (
    <main>
      <CustomSections changeColor={1}>
        <Banner />
      </CustomSections>
      <CustomSections changeColor={1}>
        <GetUnlimitedAccess />
      </CustomSections>
      <CustomSections changeColor={1}>
        <TredningCourses />
      </CustomSections>
      <CustomSections changeColor={1}>
        <BuyAccess />
      </CustomSections>
      <CustomSections changeColor={1}>
        <Categores />
      </CustomSections>
    </main>
  )
}
