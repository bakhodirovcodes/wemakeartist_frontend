import CustomSections from "@/components/UI/CustomSections/CustomSection"
import TrainersBanner from "@/components/UI/Sections/TrainersBanner/TrainersBanner"
import ClassesInMMA from "@/components/UI/Sections/ClassesInMMA/ClassesInMMA"
import Members from "@/components/UI/Sections/Members/Members"
import Lessons from "@/components/UI/Sections/Lessons/Lessons"

const Trainers = () => {
  return (
    <>
      {/* <CustomSections changeColor={1}> */}
      <TrainersBanner />
      {/* </CustomSections> */}
      <CustomSections changeColor={1}>
        <Lessons />
      </CustomSections>
      <CustomSections changeColor={1}>
        <ClassesInMMA />
      </CustomSections>
      <CustomSections changeColor={1}>
        <Members />
      </CustomSections>
    </>
  )
}

export default Trainers
