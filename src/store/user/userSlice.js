import { createSlice } from "@reduxjs/toolkit"

export const userSlice = createSlice({
  name: "counter",
  initialState: {
    isOpenModal: false,
  },
  reducers: {
    setOpenReModal: (state) => {
      state.isOpenModal = !state.isOpenModal
    },
    setOpenFalse: (state) => {
      state.isOpenModal = false
    },
    setOpenTrue: (state) => {
      state.isOpenModal = true
    },
  },
})
export const { setOpenReModal, setOpenFalse, setOpenTrue } = userSlice.actions
export default userSlice.reducer
