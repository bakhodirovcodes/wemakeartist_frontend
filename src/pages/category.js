import SEO from "@/components/SEO"
import CategoriesPage from "@/components/views/Categories/CategoriesPage"

export default function Categories() {
  return (
    <>
      <SEO />
      <CategoriesPage />
    </>
  )
}
