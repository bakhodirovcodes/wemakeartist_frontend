import SEO from "@/components/SEO"
import Trainers from "@/components/views/Trainers/Trainers"

export default function Trainer() {
  return (
    <>
      <SEO />
      <Trainers />
    </>
  )
}
