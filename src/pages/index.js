import { Main } from "@/components/views/Main/Main"
import SEO from "@/components/SEO"

export default function Home({ data }) {
  return (
    <>
      <SEO />
      <Main />
    </>
  )
}
